{
  "openapi": "3.0.0",
  "info": {
    "title": "Extractor",
    "version": "2.0",
    "description": "Extractor API v2\nThe Engine API is an HTTP API served by Docker Engine. It is the API the Docker client uses to communicate with the Engine, so everything the Docker client can do can be done with the API.\n\nMost of the client's commands map directly to API endpoints (e.g. `docker ps` is `GET /containers/json`). The notable exception is running containers, which consists of several API calls.\n\n# Get Started\n\nblablabla how to get started\n\n## Sub title\n\ncoucou le sub\n\n\n# Errors\n\nThe API uses standard HTTP status codes to indicate the success or failure of the API call. The body of the response will be JSON in the following format:\n\n```\n{\n  \"message\": \"page not found\"\n}\n```\n\n# Versioning\n\nThe API is usually changed in each release of Docker, so API calls are versioned to ensure that clients don't break.\n\nFor Docker Engine 1.13, the API version is 1.25. To lock to this version, you prefix the URL with `/v1.25`. For example, calling `/info` is the same as calling `/v1.25/info`.\n\nEngine releases in the near future should support this version of the API, so your client will continue to work even if it is talking to a newer Engine.\n\nIn previous versions of Docker, it was possible to access the API without providing a version. This behaviour is now deprecated will be removed in a future version of Docker.\n\nThe API uses an open schema model, which means server may add extra properties to responses. Likewise, the server will ignore any extra query parameters and request body properties. When you write clients, you need to ignore additional properties in responses to ensure they do not break when talking to newer Docker daemons.\n\nThis documentation is for version 1.25 of the API, which was introduced with Docker 1.13. Use this table to find documentation for previous versions of the API:\n\nDocker version  | API version | Changes\n----------------|-------------|---------\n1.12.x | [1.24](https://docs.docker.com/engine/api/v1.24/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-24-api-changes)\n1.11.x | [1.23](https://docs.docker.com/engine/api/v1.23/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-23-api-changes)\n1.10.x | [1.22](https://docs.docker.com/engine/api/v1.22/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-22-api-changes)\n1.9.x | [1.21](https://docs.docker.com/engine/api/v1.21/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-21-api-changes)\n1.8.x | [1.20](https://docs.docker.com/engine/api/v1.20/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-20-api-changes)\n1.7.x | [1.19](https://docs.docker.com/engine/api/v1.19/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-19-api-changes)\n1.6.x | [1.18](https://docs.docker.com/engine/api/v1.18/) | [API changes](https://docs.docker.com/engine/api/version-history/#v1-18-api-changes)\n\n# Authentication\n\nAuthentication for registries is handled client side. The client has to send authentication details to various endpoints that need to communicate with registries, such as `POST /images/(name)/push`. These are sent as `X-Registry-Auth` header as a Base64 encoded (JSON) string with the following structure:\n\n```\n{\n  \"username\": \"string\",\n  \"password\": \"string\",\n  \"email\": \"string\",\n  \"serveraddress\": \"string\"\n}\n```\n\nThe `serveraddress` is a domain/IP without a protocol. Throughout this structure, double quotes are required.\n\nIf you have already got an identity token from the [`/auth` endpoint](#operation/SystemAuth), you can just pass this instead of credentials:\n\n```\n{\n  \"identitytoken\": \"9cbaf023786cd7...\"\n}\n```",
    "contact": {
      "name": "Anne-Sophie Tanguy",
      "email": "anne-sophie.tanguy@golem.ai"
    }
  },
  "servers": [
    {
      "url": "http://localhost:5000",
      "description": "localhost"
    },
    {
      "url": "https://preprod.extractor.dev.golem.ai/",
      "description": "preprod"
    },
    {
      "url": "https://abbyy.extractor.golem.ai/",
      "description": "prod"
    }
  ],
  "paths": {
    "/healthcheck": {
      "get": {
        "summary": "Your GET endpoint",
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "type": "string",
                  "default": "{\"status\": \"ok\"}"
                },
                "examples": {
                  "healthcheck response": {
                    "value": {
                      "status": "ok"
                    }
                  }
                }
              }
            }
          }
        },
        "operationId": "get-health-check",
        "description": "Returns \"OK\" if the API is running.",
        "tags": [
          "healthcheck"
        ]
      },
      "parameters": []
    },
    "/v2/analyse": {
      "post": {
        "summary": "",
        "operationId": "post-analyse",
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "title": "AnalyseResponse.v1",
                  "type": "object",
                  "x-examples": {},
                  "description": "Analyse's route reponse model",
                  "properties": {
                    "content": {
                      "type": "string",
                      "description": "Text extracted from the file",
                      "default": "\"\""
                    },
                    "executionTime": {
                      "type": "string",
                      "description": "Time the Extractor took to parse the file"
                    },
                    "fileName": {
                      "type": "string",
                      "description": "File's name"
                    },
                    "nbPages": {
                      "type": "integer",
                      "description": "Number of pages of the file"
                    },
                    "ocrUsed": {
                      "type": "string",
                      "enum": [
                        "true",
                        "false",
                        "partial"
                      ],
                      "description": "Indicates if an OCR was used"
                    },
                    "request": {
                      "title": "AnalyseRequestResponse",
                      "type": "object",
                      "description": "Analyse's route request's reponse model",
                      "x-examples": {},
                      "properties": {
                        "file": {
                          "title": "FileResponse",
                          "type": "object",
                          "description": "Analyse's route file's reponse model",
                          "properties": {
                            "name": {
                              "type": "string",
                              "description": "File's name"
                            },
                            "extension": {
                              "type": "string",
                              "description": "File's extension"
                            },
                            "mimeType": {
                              "type": "string",
                              "description": "File's MIME type"
                            }
                          }
                        },
                        "options": {
                          "title": "AnalyseOptions",
                          "type": "object",
                          "x-examples": {},
                          "description": "Analyse's route option model",
                          "properties": {
                            "images": {
                              "type": "boolean",
                              "default": "false"
                            },
                            "ocr": {
                              "type": "string",
                              "default": "auto",
                              "enum": [
                                "auto",
                                "true",
                                "false"
                              ]
                            },
                            "ocrLanguage": {
                              "type": "string",
                              "default": "None",
                              "enum": [
                                "French"
                              ]
                            },
                            "ocrType": {
                              "type": "string",
                              "default": "tesseract",
                              "enum": [
                                "tesseract",
                                "abbyyPremise"
                              ]
                            },
                            "splitPerBlock": {
                              "type": "boolean",
                              "default": "false"
                            },
                            "splitPerBlockMaxSize": {
                              "type": "integer",
                              "default": ".env[\"SPLIT_PER_BLOCK_MAX_SIZE\"]",
                              "example": 300
                            },
                            "splitPerPage": {
                              "type": "boolean",
                              "default": "false"
                            },
                            "unmergeCellsXLSX": {
                              "type": "boolean",
                              "default": "false"
                            },
                            "verticalXLSX": {
                              "type": "boolean",
                              "default": "false"
                            },
                            "parseHiddenSheetsXLSX": {
                              "description": "Parses hidden sheets for a XLS/XLSX/XLSM",
                              "type": "boolean"
                            },
                            "uploadToS3": {
                              "type": "boolean",
                              "description": "If True and an URL was given for file, will try to upload to the S3 the Extractor was set to."
                            }
                          }
                        }
                      }
                    },
                    "contentPerBlock": {
                      "type": "array",
                      "description": "Content per block of contentPerBlockSize",
                      "items": {
                        "$ref": "#/paths/~1v2~1analyse/post/responses/200/content/application~1json/schema/properties/contentPerPage/items"
                      }
                    },
                    "contentPerPage": {
                      "type": "array",
                      "description": "Content per page",
                      "items": {
                        "title": "ContentPerBlock",
                        "type": "object",
                        "description": "Analyse's route contentPerBlock's reponse model. Will be present only if splitPerBlock is True in the request",
                        "properties": {
                          "content": {
                            "type": "string",
                            "description": "Content's text"
                          },
                          "page": {
                            "type": "integer",
                            "description": "Content's page"
                          }
                        },
                        "required": [
                          "content",
                          "page"
                        ]
                      }
                    },
                    "verticalXLSX": {
                      "type": "string",
                      "description": "XSLX content read vertically"
                    },
                    "contentPerBlockXLSX": {
                      "description": "XLSX's content per block",
                      "type": "array",
                      "items": {
                        "$ref": "#/paths/~1v2~1analyse/post/responses/200/content/application~1json/schema/properties/contentPerPage/items"
                      }
                    },
                    "contentPerBlockVerticalXLSX": {
                      "description": "XLSX's vertical content per block ",
                      "type": "array",
                      "items": {
                        "$ref": "#/paths/~1v2~1analyse/post/responses/200/content/application~1json/schema/properties/contentPerPage/items"
                      }
                    },
                    "images": {
                      "type": "array",
                      "description": "Array of images extracted from the doc",
                      "items": {
                        "title": "Images",
                        "type": "object",
                        "description": "Analyse's route images' reponse model",
                        "properties": {
                          "data": {
                            "type": "string",
                            "default": "Base64 Encoded image content",
                            "description": "Image's data stream"
                          },
                          "extension": {
                            "type": "string",
                            "enum": [
                              "JPEG",
                              "PNG"
                            ],
                            "description": "Image's extension"
                          }
                        },
                        "required": [
                          "data",
                          "extension"
                        ]
                      }
                    }
                  },
                  "required": [
                    "content",
                    "executionTime",
                    "fileName",
                    "nbPages",
                    "ocrUsed",
                    "request"
                  ]
                },
                "examples": {
                  "Example": {
                    "value": {
                      "fileName": "laFile.txt",
                      "content": "Heya",
                      "contentPerPage": [
                        {
                          "content": "Heya",
                          "page": 1
                        }
                      ],
                      "contentPerBlock": [
                        {
                          "content": "Heya",
                          "page": 1
                        }
                      ],
                      "nbPages": 1,
                      "request": {
                        "file": {
                          "name": "laFile.txt",
                          "extension": "txt"
                        },
                        "options": {
                          "images": true,
                          "ocr": "auto",
                          "splitPerBlock": true,
                          "splitPerBlockMaxSize": 300,
                          "splitPerPage": true,
                          "ocrType": "tesseract",
                          "ocrLanguage": null,
                          "unmergedCellsXLSX": false,
                          "verticalXLSX": false
                        }
                      },
                      "images": [
                        {
                          "data": "laDataBase64Encoded",
                          "extension": "JPEG"
                        }
                      ],
                      "ocrUsed": "false",
                      "executionTime": "0.01s"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Bad Request",
            "content": {
              "application/json": {
                "schema": {
                  "title": "AnalyseError",
                  "type": "object",
                  "description": "Analyse's route error model",
                  "properties": {
                    "fileName": {
                      "type": "string",
                      "description": "File's name"
                    },
                    "message": {
                      "type": "string",
                      "description": "Error message"
                    },
                    "analyseRequest": {
                      "$ref": "#/paths/~1v2~1analyse/post/responses/200/content/application~1json/schema/properties/request"
                    },
                    "stacktrace": {
                      "type": "string",
                      "description": "Error's stacktrace"
                    },
                    "errorType": {
                      "type": "string",
                      "description": "Error's type"
                    },
                    "moduleName": {
                      "type": "string",
                      "description": "Module where the error happened"
                    },
                    "functionName": {
                      "type": "string",
                      "description": "Function where the error happened"
                    }
                  },
                  "required": [
                    "fileName",
                    "message",
                    "analyseRequest",
                    "stacktrace",
                    "errorType"
                  ]
                }
              }
            }
          }
        },
        "description": "Will parse a file, use an OCR if needed/asked, and return the file's text. It can also return the images if asked.",
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "title": "AnalyseRequest",
                "type": "object",
                "description": "Analyse's route request model",
                "x-examples": {},
                "properties": {
                  "fileToProcess": {
                    "title": "FileData",
                    "type": "object",
                    "description": "File data sent through a request route",
                    "properties": {
                      "data": {
                        "type": "object",
                        "description": "File's data stream Or downloadable URL"
                      }
                    },
                    "required": [
                      "data"
                    ]
                  },
                  "ocr": {
                    "type": "string",
                    "default": "auto",
                    "enum": [
                      "auto",
                      "true",
                      "false"
                    ],
                    "description": "Force use of OCR for PDFs"
                  },
                  "useFront": {
                    "type": "boolean",
                    "default": "false",
                    "description": "Reponse will be returned as HTML if true"
                  },
                  "extractImages": {
                    "type": "boolean",
                    "default": "false",
                    "description": "Extract images from a document"
                  },
                  "ocrType": {
                    "type": "string",
                    "default": "tesseract",
                    "enum": [
                      "tesseract",
                      "abbyyPremise"
                    ],
                    "description": "Specify the OCR used"
                  },
                  "ocrLanguage": {
                    "type": "string",
                    "default": "French",
                    "description": "Specify the OCR language. None is defaulted to French. Only available for Abbyy atm."
                  },
                  "splitPerPage": {
                    "type": "boolean",
                    "default": "false",
                    "description": "Split the text by pages. Response adds `contentPerPage`"
                  },
                  "splitPerBlock": {
                    "type": "boolean",
                    "default": "false",
                    "description": "Will split the text by blocks of size `splitBlockMaxSize`. Response adds `contentPerBlock`"
                  },
                  "splitPerBlockMaxSize": {
                    "type": "integer",
                    "default": ".env[\"SPLIT_PER_BLOCK_MAX_SIZE\"]",
                    "example": "300",
                    "description": "Max size of block of `contentPerBlock`"
                  },
                  "verticalXLSX": {
                    "type": "boolean",
                    "default": "false",
                    "description": "With a XLSX given, will read the file vertically. Response adds `verticalXLSX`.\nWhen `splitPerBlockXLSX`is True, response adds `contentPerBlockVerticalXLSX`."
                  },
                  "unmergedCellsXLSX": {
                    "type": "boolean",
                    "default": "false",
                    "description": "Unmerge XLSX cells (duplicates data)."
                  },
                  "splitPerBlockXLSX": {
                    "type": "string",
                    "default": "False",
                    "description": "Split a XLSX in blocks by column/row.\nSet limits with `splitPerBlockXLSXColumnLimit`and `splitPerBlockXLSXRowLimit`\nResponse adds `contentPerBlockXLSX`."
                  },
                  "splitPerBlockXLSXColumnLimit": {
                    "type": "integer",
                    "default": "__DEFAULT_SPLIT_PER_BLOCK_XLSX_COLUMN_LIMIT",
                    "description": "Set the column limit of `splitPerBlockXLSX`."
                  },
                  "splitPerBlockXLSXRowLimit": {
                    "type": "integer",
                    "default": "__DEFAULT_SPLIT_PER_BLOCK_XLSX_ROW_LIMIT",
                    "description": "Set the row limit of `splitPerBlockXLSX`."
                  },
                  "parseHiddenSheetsXLSX": {
                    "type": "boolean",
                    "description": "Parses hidden sheets for a XLS/XLSX/XLSM"
                  },
                  "uploadToS3": {
                    "type": "boolean",
                    "description": "If True and an URL was given for file, will try to upload to the S3 the Extractor was set to."
                  }
                },
                "required": [
                  "fileToProcess"
                ]
              },
              "examples": {
                "Example": {
                  "value": {
                    "fileToProcess": "File Object",
                    "ocr": "auto",
                    "useFront": "false",
                    "extractImages": true,
                    "ocrType": "tesseract",
                    "ocrLanguage": "French",
                    "splitPerPage": true,
                    "splitPerBlock": true,
                    "splitPerBlockMaxSize": 300,
                    "verticalXLSX": false,
                    "unmergedCellsXLSX": false
                  }
                }
              }
            }
          }
        },
        "tags": [
          "analyse"
        ]
      },
      "parameters": []
    },
    "/logs": {
      "get": {
        "summary": "Your GET endpoint",
        "tags": [
          "logs"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "operationId": "get-logs",
        "description": "Get the logs of the Extractor"
      }
    },
    "/libsVersions": {
      "get": {
        "summary": "Your GET endpoint",
        "tags": [
          "libsVersions"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "title": "libsVersions",
                  "type": "object",
                  "properties": {
                    "pythonVersion": {
                      "type": "string",
                      "description": "Python version"
                    },
                    "pipVersion": {
                      "type": "string",
                      "description": "Pip version"
                    },
                    "pipList": {
                      "type": "array",
                      "description": "Pip list of libs versions",
                      "items": {
                        "$ref": "#/paths/~1libsVersions/get/responses/200/content/application~1json/schema"
                      }
                    }
                  },
                  "description": "Gather info on the libraries versions used"
                }
              }
            }
          }
        },
        "operationId": "get-libsVersions",
        "description": "Get the library versions installed on the server"
      }
    }
  },
  "components": {
    "schemas": {}
  }
}